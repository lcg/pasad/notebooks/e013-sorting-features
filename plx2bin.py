import click
import plx
from pathlib import Path


@click.command()
@click.argument('input-path', type=click.Path(exists=True, readable=True, dir_okay=False))
@click.argument('output-path', type=click.Path(writable=True, file_okay=False))
def main(input_path: str, output_path: str):
    Path(output_path).mkdir(parents=True, exist_ok=True)
    plx.PlxFile(input_path, export_base_dir=output_path).export_file()


if __name__ == "__main__":
    main()
